{
  description = "mkssl";
  inputs = rec {
    devshell.url = "github:numtide/devshell/main";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs =
    { self
    , devshell
    , nixpkgs
    , flake-utils
    , ...
    }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
    let
      flake-overlays = map (x: x.overlays.default) [
        devshell
      ];

      pkgs = import nixpkgs { inherit system; overlays = flake-overlays; };
      packageName = "mkssl";
      app = pkgs.callPackage ./default.nix { };
    in
    rec {
      defaultPackage = self.packages.${system}.${packageName};
      packages.${packageName} = app;

      # nix develop
      devShell = pkgs.devshell.mkShell {
        env = [
        ];
        commands = [
          {
            name = "update-flakes";
            command = "make update-all \"$@\"";
            help = "Update all flakes";
            category = "Nix";
          }
        ];
        packages = with pkgs;[
          bash
          binutils
          gawk
          gnugrep
          gnused
          nssTools # Provides certutil
          openssl
        ];
      };
    }
    );

}
