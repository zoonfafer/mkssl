# See: https://nixos.wiki/wiki/Shell_Scripts#Packaging
#
{ pkgs
, lib
, stdenvNoCC
, makeWrapper
}:
stdenvNoCC.mkDerivation rec {
  pname = "mkssl";
  version = "0.0.1";
  src = ./.;
  buildInputs = with pkgs; [
    bash
    binutils
    gawk
    gnugrep
    gnused
    nssTools # Provides certutil
    openssl
  ];
  nativeBuildInputs = [ makeWrapper ];
  installPhase = ''
    mkdir -p $out/bin
    cp bin/mkssl $out/bin/mkssl
    wrapProgram $out/bin/mkssl \
      --prefix PATH : ${lib.makeBinPath buildInputs}
  '';
}
